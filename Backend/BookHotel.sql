-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 03:50 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BookHotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `Booker`
--

CREATE TABLE `Booker` (
  `BookerID` int(11) NOT NULL,
  `BookerName` varchar(50) NOT NULL,
  `BookerSurname` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Booker`
--

INSERT INTO `Booker` (`BookerID`, `BookerName`, `BookerSurname`, `Username`, `Password`, `IsAdmin`) VALUES
(1, 'Settawut', 'Kulpatrakorn', 'khetzeiei', '123456', 1),
(2, 'Setx', 'Eiei', '', '', 0),
(3, 'Mr', 'Setx', 'Setx', '$2b$10$4pXjpijk.KxQHsu07wj1Fuaj2ywsHpgns2oLD0.kAW4zx1IRCGYPa', 1),
(6, 'gfgf', 'hggh', 'set', '$2b$10$Q5VIs18UDbJ4Ayn/iHq7Y.WoozMzuGHkWNMn4kQDrQwzjMSrP7DOK', 0),
(17, 'TEST', 'TEST', 'notadmin', '$2b$10$kNKfBawXCU/gYnpd2LF94.xAhNCRJ60ChOmIYmVZmo8JxR3rJf2WW', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Booking`
--

CREATE TABLE `Booking` (
  `BookingID` int(11) NOT NULL,
  `BookerID` int(11) NOT NULL,
  `RoomID` int(11) NOT NULL,
  `Checkin` datetime NOT NULL,
  `Night` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Booking`
--

INSERT INTO `Booking` (`BookingID`, `BookerID`, `RoomID`, `Checkin`, `Night`) VALUES
(1, 1, 1, '2022-03-24 23:13:19', 1),
(2, 2, 4, '2022-03-24 23:13:19', 3),
(4, 3, 2, '2022-03-25 00:34:11', 5),
(5, 3, 2, '2022-03-25 17:32:02', 5),
(8, 3, 2, '2022-03-26 00:00:00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `Room`
--

CREATE TABLE `Room` (
  `RoomID` int(11) NOT NULL,
  `RoomName` varchar(200) NOT NULL,
  `RoomPrice` varchar(100) NOT NULL,
  `RoomType` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Room`
--

INSERT INTO `Room` (`RoomID`, `RoomName`, `RoomPrice`, `RoomType`) VALUES
(1, 'MINI', '599 ', 'Single Bedded Room'),
(2, 'KUKIKUKI', '799', 'Double Bedded Room'),
(3, 'WONDER', '999', 'Triple Beded Room'),
(4, 'YAHOO', '999', 'Triple Bedded Room'),
(7, 'FINFIN', '599', 'Single Beded Room'),
(8, 'FANTASTIC', '599', 'Single Beded Room'),
(10, 'TEST', '99', 'TEST');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Booker`
--
ALTER TABLE `Booker`
  ADD PRIMARY KEY (`BookerID`);

--
-- Indexes for table `Booking`
--
ALTER TABLE `Booking`
  ADD PRIMARY KEY (`BookingID`),
  ADD KEY `BookerID` (`BookerID`),
  ADD KEY `RoomID` (`RoomID`);

--
-- Indexes for table `Room`
--
ALTER TABLE `Room`
  ADD PRIMARY KEY (`RoomID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Booker`
--
ALTER TABLE `Booker`
  MODIFY `BookerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `Booking`
--
ALTER TABLE `Booking`
  MODIFY `BookingID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `Room`
--
ALTER TABLE `Room`
  MODIFY `RoomID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Booking`
--
ALTER TABLE `Booking`
  ADD CONSTRAINT `BookerID` FOREIGN KEY (`BookerID`) REFERENCES `Booker` (`BookerID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `RoomID` FOREIGN KEY (`RoomID`) REFERENCES `Room` (`RoomID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
