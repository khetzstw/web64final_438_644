const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10


const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'hotel',
    password : 'hotel',
    database: 'BookHotel'
    
});


connection.connect();
const express = require('express')
const app = express()
const port = 4000

function authenticateToken(req,res,next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token==null) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET,(err,user) => {
        if(err) { return res.sendStatus(403) }
        else{
            req.user=user
            next()
        }
    })
}


app.get("/list_booking",authenticateToken, (req,res) =>{
    let user_profile = req.user 
    let booker_id = req.user.user_id
    if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
    else{
    let query = `SELECT Booker.BookerID, Booker.BookerName, Booker.BookerSurname,
                    Room.RoomName, Room.RoomPrice, Room.RoomType, Booking.Night, Booking.Checkin 
             FROM Booker,Booking,Room
            WHERE (Booking.BookerID = Booker.BookerID) AND 
            (Booking.RoomID = Room.RoomID);`;


    connection.query( query,(err,rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error querying from running db"
            
            })
        }else {
            res.json(rows);
        }
    });
    }
})



app.get("/list_booking_booker",authenticateToken, (req,res) =>{

    let user_profile = req.user 
    let booker_id = req.user.user_id
    if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
    else{

    let booker_id = req.query.booker_id
    
    let query = `SELECT Booker.BookerID, Booker.BookerName, Booker.BookerSurname,
                    Room.RoomName, Room.RoomPrice, Room.RoomType, Booking.Night, Booking.Checkin 
             FROM Booker,Booking,Room
            WHERE (Booking.BookerID = Booker.BookerID) AND 
            (Booking.RoomID = Room.RoomID) AND
            (Booker.BookerID = ${booker_id});`;


    connection.query( query,(err,rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error querying from running db"
            
            })
        }else {
            res.json(rows);
        }
    });
}
})


app.get("/list_booking_room",authenticateToken, (req,res) =>{

    let user_profile = req.user 
    let booker_id = req.user.user_id
    if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
    else{

    let room_id = req.query.room_id
    
    let query = `SELECT Booker.BookerID, Booker.BookerName, Booker.BookerSurname,
                    Room.RoomName, Room.RoomPrice, Room.RoomType, Booking.Night, Booking.Checkin 
             FROM Booker,Booking,Room
            WHERE (Booking.BookerID = Booker.BookerID) AND 
            (Booking.RoomID = Room.RoomID) AND
            (Room.RoomID = ${room_id});`;


    connection.query( query,(err,rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error querying from running db"
            
            })
        }else {
            res.json(rows);
        }
    });
}
})




app.post("/register_booker", (req, res) =>{
    
    let booker_name = req.query.booker_name
    let booker_surname = req.query.booker_surname
    let booker_username = req.query.booker_username
    let booker_password = req.query.booker_password


    bcrypt.hash(booker_password, SALT_ROUNDS, (err,hash)=> {

            let query =  `INSERT INTO Booker
                                    (BookerName, BookerSurname, Username, Password, IsAdmin) 
                                    VALUES ('${booker_name}', '${booker_surname}',
                                            '${booker_username}' , '${hash}', false)`

                console.log(query)

                connection.query( query, (err, rows) => {
                    if (err){
                        res.json({
                                    "status" : "400", 
                                    "message" : "Error inserting data into db"
                                })
                    }else{
                        res.json({
                            "status" : "200", 
                            "message" : "Adding new booker successful"
                        })
                    }
                });
    })  
});



app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query =`SELECT * FROM Booker WHERE Username='${username}'`
   
    connection.query( query, (err, rows) => {
            if (err){
                console.log(err)
                res.json({
                            "status" : "400", 
                            "message" : "Error querying from booking db"
                        })
            }else{

                let db_password = rows[0].Password
                bcrypt.compare(user_password, db_password, (err,result)=> {
                    if (result) {
                        let payload = {
                            "username" : rows[0].Username,
                            "user_id" : rows[0].BookerID,
                            "IsAdmin" : rows[0].IsAdmin
                        }
                        console.log(payload)
                        let token = jwt.sign(payload, TOKEN_SECRET,{expiresIn:'1d'} )
                        res.send(token)
                    }else {res.send("Invalid username / password") }
                })                
            }
        })
})        


app.post("/booking",authenticateToken, (req, res) =>{
     
    let booker_id = req.query.booker_id
    let room_id = req.query.room_id
    let night = req.query.night
    let checkin = req.query.checkin

    let query =  `INSERT INTO Booking
                        (BookerID, RoomID, Night, Checkin) 
                        VALUES ('${booker_id}',
                                '${room_id}',
                                ' ${night}',
                                 '${checkin}')`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Booking room successful"
            })
        }
    });

});






app.post("/add_room",authenticateToken, (req, res) =>{

    let user_profile = req.user 
    let booker_id = req.user.user_id

    if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
    else{
    let room_name = req.query.room_name
    let room_price = req.query.room_price
    let room_type = req.query.room_type
    let query =  `INSERT INTO Room 
                        (RoomName, RoomPrice, RoomType) 
                        VALUES ('${room_name}', '${room_price}', '${room_type}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Adding room successful"
            })
        }
        });

    }
})

app.post("/delete_room",authenticateToken, (req, res) =>{
    let user_profile = req.user 
    let booker_id = req.user.user_id

    if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
    else{
    let room_id = req.query.room_id   
    let query =  `DELETE FROM Room WHERE RoomID=${room_id}`
                    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error deleting room"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Deleting room successful"
            })
        }
        });
    }

})


app.post("/update_room",authenticateToken, (req, res) =>{
    let user_profile = req.user 
    let booker_id = req.user.user_id

    if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
    else{
    let room_id = req.query.room_id
    let room_name = req.query.room_name
    let room_price = req.query.room_price
    let room_type = req.query.room_type

    let query =  `UPDATE Room SET
                    RoomName='${room_name}',
                    RoomPrice='${room_price}',
                    RoomType='${room_type}'
                    WHERE RoomID=${room_id}`
                    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error updating room"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Updating room successful"
            })
        }
        });
    }
    })

    


    app.post("/delete_booker",authenticateToken, (req, res) =>{
        let user_profile = req.user 
        let booker_id = req.user.user_id

        if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
        else{
        
        let booker_id = req.query.booker_id
        

    
        let query =  `DELETE FROM Booker WHERE BookerID=${booker_id}`
                        
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err){
                res.json({
                            "status" : "400", 
                            "message" : "Error deleting record"
                        })
            }else{
                res.json({
                    "status" : "200", 
                    "message" : "Deleting record successful"
                })
            }
            });
    
        }
    })
    

    app.post("/delete_booking",authenticateToken, (req, res) =>{
        let user_profile = req.user 
        let booker_id = req.user.user_id

        if(!req.user.IsAdmin){ res.send("Unauthorized Because you're not admin") }
        else{
       
        let booking_id = req.query.booking_id
        
    
        let query =  `DELETE FROM Booking WHERE BookingID=${booking_id}`
                        
        console.log(query)
    
        connection.query( query, (err, rows) => {
            if (err){
                res.json({
                            "status" : "400", 
                            "message" : "Error deleting booking"
                        })
            }else{
                res.json({
                    "status" : "200", 
                    "message" : "Deleting booking successful"
                })
            }
            });
    
        }
    })


app.listen(port, () => {
    console.log("Now starting Booking Hotel Backend at port "+port)

})