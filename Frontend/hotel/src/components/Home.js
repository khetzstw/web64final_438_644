import * as React from 'react';

import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

import Logo from './images/Logo.png'



function Home() {
    const [room_type, setType, night, setNight] = React.useState('');
    const [value, setValue] = React.useState(null);

    const handleChange = (event) => {
        setType(event.target.value);
        setNight(event.target.value);

    };

    return (
        <Box sx={{
            mx: 55,
            my: 1,
            mt: 10,
            size : 20
        }} > 

            <Paper elevation={10} >

            <br />

                <br />
                <center>
                    <img src={Logo} style={{ width: "250px" }} />
                </center>

                <h1 className="h1" > Hotel hub </h1>

                <br />
            </Paper>
        </Box>
    );

}

export default Home;