import {Link} from "react-router-dom"

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import { autocompleteClasses } from "@mui/material";


function Header() {
  return (
    <Box sx={{ flexGrow: 0.6 }}>
      <AppBar position="static">
        <Toolbar>

        <Typography variant="h5">
        &nbsp; &nbsp; Hotel hub
        
         </Typography>
         &nbsp; &nbsp; 
         <Link to="Home">
         
                <Button fullWidth variant="contained" 
                sx={{ mx: 8, my: 2 }}>
                <Typography> Home </Typography>
            </Button>

        </Link>
        &nbsp; &nbsp; 
        <Link to="Room">
                <Button  fullWidth variant="contained" 
                sx={{ mx: 8, my: 2 }}>
                <Typography> Room </Typography>
            </Button>

        </Link>
        &nbsp; &nbsp; 

        <Link to="Booking">
                <Button  fullWidth variant="contained" 
                sx={{ mx: 8, my: 2}}>
                <Typography> Booking </Typography>
            </Button>

        </Link>

        &nbsp; &nbsp; 
      

        &nbsp; &nbsp; 
         <Link to="Login">
         
                <Button fullWidth variant="contained" 
                sx={{ mx: 8, my: 2}}>
                <Typography> Login </Typography>
            </Button>

        </Link>
        &nbsp; &nbsp; 

        <Link to="SignUp">
                <Button  fullWidth variant="contained" 
                sx={{ mx: 8, my: 2}}>
                <Typography> SignUp </Typography>
            </Button>

        </Link>

        
    
        </Toolbar>
            
      </AppBar>
    </Box>
  );
}
export default Header;


