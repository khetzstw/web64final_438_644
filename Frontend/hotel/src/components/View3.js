import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Button  from '@mui/material/Button'
import SvgIcon from '@mui/material/SvgIcon';
import NetworkWifiIcon from '@mui/icons-material/NetworkWifi';

import Room3 from './images/Room3.jpg'
import { ForkLeft } from '@mui/icons-material';



function View3 (props){


    return (
        <div>
        <Box sx={{ mx: 40, my: 1, mt:5}}>
        <Paper elevation={20} >
        <br/>

   
                <center>
                    <img src={Room3} style={{width : "800px"}}/>  
                    <h1 style={{marginTop : "10px"}}>Triple Bedded Room</h1>
                    <h3 className = "a1" > Max persons : 3  </h3>

                    <Box sx={{ width : 250, my: 1,}}>
                    <Paper elevation={10} >
                      <h2 style={{marginTop : "10px"}}>Price 999 Bath </h2>
                      </Paper>
                      </Box>
                    <h4 style={{marginTop : "5px"}}>✓Desk        ✓Refrigerator    ✓Telephone    </h4>
                    <h4 style={{marginTop : "1px"}}>✓Air Conditioning     ✓TV     ✓Balcony       </h4>
                    <h4 style={{marginTop : "1px"}}>✓ Non-smoking       ✓City view      </h4>
                    <h1 style={{marginTop : "10px"}}>Facilities</h1>

                <Box sx={{ width : 300, my: 1}}>
                
                <Paper elevation={3} >
                <h3 className = "a1" >  Free Cancellation </h3>
                </Paper>
                              
                <Paper elevation={3} >
                <h3 className = "a1" > Free WIFI </h3>
                </Paper>   

                <Paper elevation={3} >
                <h3 className = "a1" > Car park   </h3>
                </Paper>   

                <Paper elevation={3} >
                <h3 className = "a1" >  Sauna </h3>
                </Paper>    

                 <Paper elevation={3} >
                <h3 className = "a1" >  Front desk [24-hour]</h3>
                </Paper>   

                                       
                                
                
                </Box>

                </center>

               
    <br/>
                </Paper>
    </Box>

<Button 
variant="contained" 
href="/Room"
style={{marginTop : "30px", 
       fontFamily : "Century Gothic" , 
       fontSize : "18px", 
       fontWeight : "bold", 
       marginBottom : 20 }}> 
Back

</Button>
</div>
    );
}

export default View3;