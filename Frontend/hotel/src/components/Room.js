import Room1 from './images/Room1.jpg'
import Room2 from './images/Room2.jpg'
import Room3 from './images/Room3.jpg'

import Button  from '@mui/material/Button'
import {Link} from "react-router-dom"
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';


function Room () {

    return (
        <div> 
<br/>
            <h1 style={{marginTop : "10px"}}>Rooms</h1>
            
            <div>
            <Box sx={{ mx: 55, my: 1}}>
            <Paper elevation={20} >
            <br/>
                    <center>
                        <img src={Room1} style={{width : "800px"}}/>  
                    </center>
                    
                  <h2>  Single Bedded Room </h2>
                  
                    <Button 
                      variant="contained" 
                      href="/View1"
                      style={{marginTop : "5px", 
                             fontFamily : "Century Gothic" , 
                             fontSize : "15px", 
                             fontWeight : "bold", 
                             marginBottom : 20 }}> 
                      View 
                      
                    </Button>
                    </Paper>
                    </Box>
                  
                  </div>

            <br/>

            <div>

            <Box sx={{ mx: 55, my: 1}}>
            <Paper elevation={10} >
            <br/>
                    <center>
                    <img src={Room2} style={{width : "800px"}}/>
                    </center>

                    <h2>  Double Bedded Room </h2>

                    <Button 
                      variant="contained" 
                      href="/View2"
                      style={{marginTop : "15px", 
                              fontFamily : "Century Gothic" ,
                              fontSize : "15px", 
                              fontWeight : "bold",
                              marginBottom : 20 }}> 
                     View
                    </Button>
                    </Paper>
                    </Box>
                    
            </div>

            <br/>

            <div>

            <Box sx={{ mx: 55, my: 1}}>
            <Paper elevation={20} >
            <br/>
                    <center>
                    <img src={Room3} style={{width : "800px"}}/>
                    </center>
                    
                    <h2>  Triple Bedded Room </h2>

                    <Button 
                      variant="contained" 
                      href="/View3"
                      style={{marginTop : "15px",  
                              fontFamily : "Century Gothic" ,
                              fontSize : "15px", 
                              fontWeight : "bold",
                              marginBottom : 20 }}>
                        View
                     </Button>
                    </Paper>
                    </Box>
                                           
            </div>       
        </div>
    );
}

export default Room;
