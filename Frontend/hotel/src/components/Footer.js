import Logo from './images/Logo.png'
import * as React from 'react';
function Footer (){


    return (
        <footer>
<br/>
        <div className="footer">
          <h3 style={{color: "#ffffff" }}> Contact Information </h3>
        </div>
          <div className="footer-left">
            <img src="https://img.icons8.com/external-flaticons-flat-flat-icons/64/000000/external-call-100-most-used-icons-flaticons-flat-flat-icons-2.png" style={{ width: "20px",marginBottom: -3 }} /> &nbsp; 088-8888888<br/> 
            <img src="https://img.icons8.com/fluency/48/000000/facebook-new.png" style={{ width: "20px",marginBottom: -3 }} /> &nbsp; Hotel hub  <br/> 
            <img src="https://img.icons8.com/fluency/48/000000/instagram-new.png" style={{ width: "20px",marginBottom: -4 }} /> &nbsp;  hotel_hub_888<br/>
            <img src="https://img.icons8.com/color/48/000000/line-me.png" style={{ width: "20px",marginBottom: -4 }} /> &nbsp; @hotelhub888<br/>
            
        </div>
         
        <div className="footer-right">
        <img src={Logo} style={{ width: "100px" }} />
        </div>

  </footer>
    
    );
}

export default Footer;