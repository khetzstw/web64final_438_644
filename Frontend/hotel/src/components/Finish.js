import Logo2 from './images/Logo2.png'

import Button from '@mui/material/Button'
import { Link } from "react-router-dom"
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';


function Finish() {
    return (

        <div>
            <br />
            
            <Box sx={{ mx:55, my: 1 ,  mt: 10 }}>
                <Paper elevation={20} >
                <br />
                <h2>You successfully created your booking </h2>
                    <br />
            <center>
                <img src={Logo2} style={{ width: "350px" }} />
            </center>

                    <h2>Thanks for your Booking</h2>

                    <br />
                </Paper>
            </Box>
            <br />
            <Button
                        variant="contained"
                        href="/Home"
                        style={{
                            marginTop: "5px",
                            fontFamily: "Century Gothic",
                            fontSize: "20px",
                            fontWeight: "bold",
                            color : "#FFFFFF",
                            marginBottom: 20
                        }}>
                          Back to homepage 

                    </Button>
        </div>

    );
}

export default Finish;