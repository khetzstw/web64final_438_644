import * as React from 'react';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Button  from '@mui/material/Button'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Room1 from './images/Room1.jpg'
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';



function Booking (){
    const [ room_type, setType, night, setNight, test,setTest ] = React.useState('');
    const [value, setValue] = React.useState(null);

    const handleChange = (event) => {
        setType(event.target.value);
            
};

    const handleChange1 = (event) => {
      
        setNight(event.target.value);

};
   

    return (
      <div>
<Box sx={{ mx: 30, my: 1 ,mt : 5 }}> <br /> <br /> <br />
       <Paper elevation={10} >
       <br />
            <h1> Hotel hub </h1>
        
       <br /> <br />
       <FormControl style={{marginBottom : 20, width : 635}}>
        <InputLabel>Room Type</InputLabel>
        <Select
        
          
          value={room_type}
          label="Room Type"
          onChange={handleChange}
        >
          <MenuItem value={111}>Single Bedded Room</MenuItem>
          <MenuItem value={222}>Double Bedded Room</MenuItem>
          <MenuItem value={333}>Triple Bedded Room</MenuItem>
        </Select>        
      </FormControl>
      <br />
      <FormControl style={{marginBottom : 20, width : 300}}>

      <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        label="Checkin"
        value={value}
        onChange={(newValue) => {
          setValue(newValue);
        }}
        renderInput={(params) => <TextField {...params} />}
      />
    </LocalizationProvider>
    </FormControl>

    <FormControl style={{marginBottom : 20, marginLeft : 40, width : 300}}>
        <InputLabel>Night </InputLabel>
        <Select
          
          value={night}
          label="Night"
          onChange={handleChange1}
        >
          <MenuItem value={11}>1</MenuItem>
          <MenuItem value={22}>2</MenuItem>
          <MenuItem value={33}>3</MenuItem>
          <MenuItem value={44}>4</MenuItem>
          <MenuItem value={55}>5</MenuItem>
          <MenuItem value={66}>6</MenuItem>
          <MenuItem value={77}>7</MenuItem>
        </Select>      
    </FormControl>
        
    
<br/><br/><br/><br/><br/>
    
        </Paper>
</Box>
<Button 
                      variant="contained" 
                      href="/Finish"
                      style={{marginTop : "30px",  
                              fontFamily : "Century Gothic" ,
                              fontSize : "15px", 
                              fontWeight : "bold",
                              marginBottom : 20 }}>
                        Book Now
                     </Button>
</div> );

}

export default Booking;