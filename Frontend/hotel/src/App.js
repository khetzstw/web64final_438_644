import './App.css';

import { Routes, Route } from "react-router-dom";

import Header from './components/Header';
import SignUp from './components/SignUp';
import Login from './components/Login';
import Home from './components/Home';
import Room from './components/Room';
import View1 from './components/View1';
import View2 from './components/View2';
import View3 from './components/View3';
import Booking from './components/Booking';
import Footer from './components/Footer';
import Finish from './components/Finish';

function App() {
  return (
    <div className="App">
      <Header />
     
     <Routes>
          <Route path="/Login" element ={
              <Login/>
          }/>

          <Route path="/SignUp" element ={
              <SignUp/>
          }/>

          <Route path="/Home" element ={
              <Home/>
          }/>

          <Route path="/" element ={
              <Home/>
          }/>

           <Route path="/Room" element ={
              <Room/>
              
          }/>

            <Route path="/View1" element ={
              <View1/>
              
          }/>

          <Route path="/View2" element ={
              <View2/>
              
          }/>

          <Route path="/View3" element ={
              <View3/>
              
          }/>

           <Route path="/Booking" element ={
              <Booking/>
              
          }/>

            <Route path="/Finish" element ={
              <Finish/>
              
          }/>


      </Routes>

      <br/><br/><br/><br/><br/>

      <Footer />

    </div>
  );
}

export default App;
