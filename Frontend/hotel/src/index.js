import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom";


import { createTheme, ThemeProvider } from '@mui/material/styles';
import { green, purple } from '@mui/material/colors';

const theme = createTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#3c096c',
      contrastText: '#fbf9f9',
      light: '#161515',
      dark: '#7373d0',
    },
    secondary: {
      main: '#f70808',
      contrastText: '#000000',
    },
    error: {
      main: '#8e3636',
      contrastText: '#161515',
    },
    text: {
      primary: '#000000',
      secondary: '#000000',
      disabled: '#1a1a1a',
      hint: '#000000',
    },
    divider: '#121212',
    background: {
      paper: '#f0cfed',
    },
  },
});

ReactDOM.render(
  <BrowserRouter>
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
